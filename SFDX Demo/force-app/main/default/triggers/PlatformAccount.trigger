trigger PlatformAccount on Account (after insert) {

    List<Account_Data__e> accountsList = new List<Account_Data__e>();
    for(Account acc : Trigger.new){
        Account_Data__e accountEvent = new Account_Data__e();
        accountEvent.Account_Name__c = acc.Name;
        accountsList.add(accountEvent);
    }

    List<Database.SaveResult> results = EventBus.publish(accountsList);
     
}