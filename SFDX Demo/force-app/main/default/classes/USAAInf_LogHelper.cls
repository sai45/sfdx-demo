/**********************************************************************************************************
 * Copyright (C) 2017
 * USAA
 * All Rights Reserved
 * Apex Class Name : USAAInf_LogHelper
 * Version         : 1.0
 * Created On      : August 18 2017
 * Function        : This class is used for inserting exceptions in USAAInfLog__c
 * TestClass       : USAAInf_LogutilityTest
 *
 *
 * Modification Log :
 *
 *     Version No                Developer                                 Date                                Comments
 *
 * ----------------------------------------------------------------------------------------------------------------------------
 *         1.0               Amanjot Kaur                              08/18/2017                         Initial Version
 *         2.0               Amanjot Kaur                              08/28/2017                         Implemented Code Review Comments #120899
 *         3.0               Harshith M L/Amanjot Kaur                 08/30/2017                         Bringing DLOG framework to USAA Standards 
 *         4.0               Hemanth Kashyap                           09/06/2017                         Implemented code review comments #123263                                
 ************************************************************************************************************/


public class USAAInf_LogHelper
{
    //List to store all uncommitted Logs. It will be used when commitLogs() method is called in LogUtility class.
    public static List<USAAInf_LogWrapper> lstLogWrapper = new List<USAAInf_LogWrapper>();
      
    //Eror message for exception when Label.LoggingFrameWork_LogTypes_Severity mapping is incorrect
    private static final String INCORRECT_LOG_TYPE_SEVERITY_MAPPING = UWC_GlobalConstants.SEVERITYERRORMSG;
    
    /*
    * <p>    
    * This method is going to be called from any Apex Class or Trigger Catching any Exception ( except Integration one )
    * @param         
    * @return   Boolean        This method returns Boolean value whether to store Exception.Method returns true if Logs Enabled is enabled in Controller Settings for his/her Profile.
    */
    public static Boolean getIsLoggingEnabled()
    {
        // Fetching current user details  
        Id currentUserProfileID = UserInfo.getUserId();
        
        // Fetching hierarchy custom setting record based on current user's profile 
        USAAInf_ControllerSettings__c switchSetting = USAAInf_ControllerSettings__c.getInstance(currentUserProfileID);
        if (switchSetting == null) 
        {
            switchSetting = USAAInf_ControllerSettings__c.getOrgDefaults();
        }
        return switchSetting != null && switchSetting.LogsEnabled__c;
    }
    
    /*
    * <p>    
    * This method is going to be called from any Apex Class or Trigger Catching any Integration Exception
    * @param         
    * @return   Boolean    This method returns Boolean value whether to store Integration Exception. Method returns true if Log Integration Errors is enabled in Controller Settings for his/her Profile.
    */
    public static Boolean getIsIntegrationLoggingEnabled() 
    {
        // Fetching current user details
        Id currentUserProfileID = UserInfo.getUserId();
        
        // Fetching hierarchy custom setting record based on current user's profile 
        USAAInf_ControllerSettings__c switchSetting = USAAInf_ControllerSettings__c.getInstance(currentUserProfileID);
        if (switchSetting == null)
        {
            switchSetting = USAAInf_ControllerSettings__c.getOrgDefaults();
        }
        return switchSetting != null && switchSetting.LogIntegrationErrors__c;
    }

    
    /*
    * <p>    
    * This method is going to be called from getObjectInstances method which return a log object from wrapper
    * @param    USAAInf_LogWrapper    single instance of wrapper class
    * @return   sObject               This method returns USAAInf_log values to be inserted after exception is caught.
                                      Method to Instantiate SObject used for logging errors with values in LogWrapper variables.
    */
     public static SObject getObjectInstance(USAAInf_LogWrapper logWrapperInstance) 
     {
        // Creating an instance of log object, populating it from the wrapper and returning it.
        USAAInf_Log__c logInstance = new USAAInf_Log__c();
        logInstance.Type__c = logWrapperInstance.logType;
        logInstance.Source__c = logWrapperInstance.logSource;
        logInstance.Details__c = logWrapperInstance.logDetails;
        logInstance.Severity__c = logWrapperInstance.logSeverity;
        logInstance.Timestamp__c = logWrapperInstance.logTimeStamp;
        logInstance.SourceType__c = logWrapperInstance.logSourceType;
        logInstance.Message__c = logWrapperInstance.logMessage;   
        logInstance.Method__c = logWrapperInstance.Methodname;
        logInstance.RecordID__c = logWrapperInstance.Recordid;
        logInstance.Object__c = logWrapperInstance.Objectname;
        
        return (sObject)logInstance;
     }
    
    /*
    * <p>    
    * This method is going to be called from USAAInf_logutility Class to convert list of wrapper to list of logs
    * @param     List<LogWrapper>      List of Wrapper class    
    * @return    List<sObject>         Method retuns list of SObjects from the value listLW passed as an argument.                 
    */
    public static List<SObject> getObjectInstances(List<USAAInf_LogWrapper> lstLogWrapper) 
    {
        // List to capture logs
        List<SObject> listSobjectLogs = new List<SObject>();
        
        // Iterating over list of wrapper instances and adding it to list of logs
        if (lstLogWrapper!=null && !lstLogWrapper.isEmpty()) 
        {
            for (USAAInf_LogWrapper wrapInstance : lstLogWrapper)
             {
                if (USAAInf_LogHelper.getObjectInstance(wrapInstance) != null) 
                {
                    listSobjectLogs.add(USAAInf_LogHelper.getObjectInstance(wrapInstance));
                }
            }
        }
        return listSobjectLogs;
    }
    

    

    /*   
    * This method is going to be called from USAAInf_Logutility Class to insert attachment
    * @param     SObject                Instance of sObject 
      @param     USAAInf_LogWrapper     wrapper class instance which contains PayloadFile value
    * @return    Void                   Method doesn't return anything            
    */
    public static void createAttachmentFromPayload(SObject objectInstance, USAAInf_LogWrapper wrapperInstance)
    {
        if (objectInstance.Id != null) 
        {
            if (wrapperInstance.logType == UWC_GlobalConstants.INTEGRATION && String.isNotBlank(wrapperInstance.logPayloadFile))
             {
                Attachment attachmentInstance = new Attachment();
                attachmentInstance.ParentId = objectInstance.Id;
                attachmentInstance.Body = Blob.valueOf(wrapperInstance.logPayloadFile);
                attachmentInstance.Description = UWC_GlobalConstants.ATTACHMENTMESSAGE + objectInstance.get(UWC_GlobalConstants.SOURCE) + UWC_GlobalConstants.DESCRIPTIONMESSAGE + wrapperInstance.logSourceType ;
                attachmentInstance.Name = objectInstance.get(UWC_GlobalConstants.SOURCE) + UWC_GlobalConstants.INTEGRATIONREQUESTDETAILS;
                attachmentInstance.ContentType = UWC_GlobalConstants.CONTENTTYPE;
                try 
                {
                    Insert attachmentInstance;
                } 
                catch (Exception e)
                {
                     USAAInf_LogUtility.createLogs(UWC_GlobalConstants.EXCEPTION_TYPE,UWC_GlobalConstants.EXCEPTION_SEVERITY,UWC_GlobalConstants.EXCEPTION_SOURCETYPE,UWC_GlobalConstants.LOGHELPER, e.getStackTraceString(),e.getMessage(),UWC_GlobalConstants.LOGMETHODATTACHPAYLOAD,objectInstance.Id,UWC_GlobalConstants.OBJECTLOG, JSON.serializePretty(objectInstance), true);
                }
            }
        }
    }
    
    /*   
    * This method is going to be called from USAAInf_logutility Class to return attachment
    * @param     SObject               Isntance of sObject record
      @param     USAAInf_LogWrapper    wrapper class instance which contains PayloadFile value
    * @return    Void                  Method doesn't return any value             
    */
    public static Attachment returnAttachmentFromPayload(SObject objectInstance, USAAInf_LogWrapper wrapperInstance) 
    {
        // Creating instance of attachment to be inserted
        Attachment attachmentInstance;
        
        // Valdating  log instance type for Integration and converted it to attachment. Insert attachment realted to log record
        if (objectInstance.Id != null)
        {
            if (wrapperInstance.logType == UWC_GlobalConstants.INTEGRATION && String.isNotBlank(wrapperInstance.logPayloadFile))
             {
                attachmentInstance = new Attachment();
                attachmentInstance.ParentId = objectInstance.Id;
                attachmentInstance.Body = Blob.valueOf(wrapperInstance.logPayloadFile);
                attachmentInstance.Description = UWC_GlobalConstants.ATTACHMENTMESSAGE + objectInstance.get(UWC_GlobalConstants.SOURCE) + UWC_GlobalConstants.DESCRIPTIONMESSAGE + wrapperInstance.logSourceType ;
                attachmentInstance.Name = objectInstance.get(UWC_GlobalConstants.SOURCE) + UWC_GlobalConstants.INTEGRATIONREQUESTDETAILS;
                attachmentInstance.ContentType = UWC_GlobalConstants.CONTENTTYPE;
            }
        }
        return attachmentInstance;
    }
    /*   
    * This method is going to be called from USAAInf_logutility Class to insert attachment
    * @param     SObject               Instace of sObject record
      @param     String                String value to be inserted as a text file
    * @return    void                  Method inserts a text file as an attachment to a DLOG record based on value in String logMessage.             
    */
    public static void createAttachmentFromMessage(SObject objectInstance, String logMessage)
    {
        // Validating the record and log message before converting it to attachment and inserting it
        if (objectInstance.Id != null)
        {
            if (String.isNotBlank(logMessage))
             {
                Attachment attachmentInstance = new Attachment();
                attachmentInstance.ParentId = objectInstance.Id;
                attachmentInstance.Body = Blob.valueOf(logMessage);
                attachmentInstance.Description = UWC_GlobalConstants.ATTACHMENTMESSAGE + objectInstance.get(UWC_GlobalConstants.SOURCE) + UWC_GlobalConstants.DETAILEdLOGMESSAGE;
                attachmentInstance.Name = objectInstance.get(UWC_GlobalConstants.SOURCE) +UWC_GlobalConstants.DETAILEDMSG;
                attachmentInstance.ContentType = UWC_GlobalConstants.CONTENTTYPE;
                try 
                {
                    INSERT attachmentInstance;
                } 
                catch (Exception e)
                {
                    //USAAInf_LogUtility.createLogs(UWC_GlobalConstants.EXCEPTION_TYPE,UWC_GlobalConstants.EXCEPTION_SEVERITY,UWC_GlobalConstants.EXCEPTION_SOURCETYPE,UWC_GlobalConstants.LOGHELPER, e.getStackTraceString(),e.getMessage(),UWC_GlobalConstants.EXCEPTION_DLOGMETHODATTACHMENT,objectInstance.Id,UWC_GlobalConstants.OBJECTLOG, JSON.serializePretty(objectInstance), true);
                }
            }
        }
    }
    
    /*   
     This method Method invokes parametrized LogUtility.commitLogs() method with a list of uncommited logs stored in lstLogWrapper
    * @param     NULL
    * @return    void    Doesn't return any value                             
    */
    public static void commitLogsNow()
    {
        // Committing collected logs and clearing the list
        if (!lstLogWrapper.isEmpty())
         {
            USAAInf_LogUtility.commitLogs(lstLogWrapper);
            lstLogWrapper.clear();
        }
    }
    
    /*   
      JSON serialized lstLogWrapper is passed to the method which deserealizes the string into LogWrapper class variables and the returned list is passed as a parameter to commitLogs method of LogUtility class.
    * @param     String        Serialized logwrapper
    * @return    void          Doesn't return any value                     
    */
    @future
    public static void commitLogsInFuture(String lstLogWrapperJSON) 
    {
        // List to store log wrapper which will be populated after deserializing the JSON
        List<USAAInf_LogWrapper> logWrapperList = USAAInf_LogWrapper.decodeJSON(lstLogWrapperJSON);

        // Committing logs to Salesforce database in future context
        if (!logWrapperList.isEmpty())
         {
            USAAInf_LogUtility.commitLogs(logWrapperList);
        }
    }    
}