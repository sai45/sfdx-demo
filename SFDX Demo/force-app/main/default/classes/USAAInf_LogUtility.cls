/********************************************************************************
 * Copyright (C) 2017
 * USAA
 * All Rights Reserved
Apex Class Name : USAAInf_LogUtility
Version         : 1.0
Created On      : August 18 2017
Function        : This class is used for inserting exceptions in USAAInfLog__c
TestClass       : USAAInf_LogutilityTest


Modification Log :

*     Version No                    Developer                         Date                                Comments
*
* ----------------------------------------------------------------------------------------------------------------------------
*          1.0                  Amanjot Kaur                        05/18/2017                          Initial Version
*                               
           2.0                  Amanjot Kaur                        08/28/2017                         Implemented Code Review Comments #120899
           3.0                  Harshith M L/                       08/30/2017                         Bringing DLOG framework to USAA Standards 
                                Amanjot Kaur     
           4.0                  Zach McKie                          09/19/2017                         Updated insertLogTransaction and added deleteLogTransaction 
*************************************************************************************************************/
public class USAAInf_LogUtility 
{
    //Error message if List inserting logs is empty
    private static final String COMMIT_LIST_EMPTY = UWC_GlobalConstants.ERRORMSG;
    
   /*   
    * This Method is called from all classes to  handle exception. It logs  error records in the ORG
    * @param     String                wrapper class instance which contains error Type value
    * @param     String                wrapper class instance which contains Severity value
    * @param     String                wrapper class instance which contains Source Type value
    * @param     String                wrapper class instance which contains LogSource value
    * @param     String                wrapper class instance which contains LogDetails value
    * @param     String                wrapper class instance which contains LogMessage value
    * @param     String                wrapper class instance which contains MethodName value
    * @param     String                wrapper class instance which contains Recordid value
    * @param     String                wrapper class instance which contains Object Nam value
    * @param     String                wrapper class instance which contains PayloadFile value
    * @param     Boolean               wrapper class instance which contains Boolean value to commit now
    * @return    SObject         Method inserts a text file as an attachment to log object based on value in String PayloadFile.               
    */
    public static SObject createLogs(String logType, String logSeverity, String logSourceType, 
                                     String logSource, String logDetails, String logMessage,String methodname, 
                                     String recordid, 
                                     String objectname,String logPayloadFile, Boolean executeDML)
    {
        // Boolean flag to identify whether logs should be created
        Boolean createLogs;
        
        // Creating instance of log wrapper and populate values
        USAAInf_LogWrapper logInstance= new USAAInf_LogWrapper();
        logInstance.logType = logType;
        logInstance.logSource = logSource;
        logInstance.logMessage = logMessage;
        logInstance.logTimeStamp = System.now();
        logInstance.logSourceType = logSourceType;
        logInstance.logPayloadFile = logPayloadFile;
        logInstance.logDetails = logDetails;
        logInstance.logSeverity = logSeverity;
        logInstance.Recordid = recordid;
        logInstance.Objectname = objectname;
        logInstance.Methodname = methodname;
        
        // Validate whether DML should be execute or not
        if (executeDML) 
        {
            // Creating an instance of object
            SObject sObjectInstance = USAAInf_LogHelper.getObjectInstance(logInstance);
            
            if (sObjectInstance != null) 
            {
                //check if controller setting is enabled for storing Integration Logs
                createLogs = logType == UWC_GlobalConstants.INTEGRATION ? USAAInf_LogHelper.getIsIntegrationLoggingEnabled() : USAAInf_LogHelper.getIsLoggingEnabled();
                if (createLogs) 
                {
                    try 
                    {
                        system.debug(sObjectInstance);
                        Insert sObjectInstance;
                    } 
                    catch (Exception e)
                    {
                        USAAInf_LogUtility.createLogs(UWC_GlobalConstants.EXCEPTION_TYPE,UWC_GlobalConstants.EXCEPTION_SEVERITY,UWC_GlobalConstants.EXCEPTION_SOURCETYPE,UWC_GlobalConstants.LOGUTILITY,e.getStackTraceString(),e.getMessage(),UWC_GlobalConstants.EXCEPTION_DLOGMETHODCOMMIT,'','',JSON.serializePretty(sObjectInstance),true); 
                         //Return if the failure occurs after sending email. 
                         return null;
                    }
                    // Creating attachment from payload or message for Integration exceptions
                    if (String.isNotBlank(logPayloadFile)) 
                    {
                        USAAInf_LogHelper.createAttachmentFromPayload(sObjectInstance, logInstance);
                    }
                    if (String.isNotBlank(logMessage) && logMessage.length() > 1000)
                    {
                        USAAInf_LogHelper.createAttachmentFromMessage(sObjectInstance, logMessage);
                    }

                    System.debug('sObjectInstance ->'+sObjectInstance);
                    //Return log instance on Succesful Insertion
                    return sObjectInstance;
                }

            }
            
        } 
        else 
        {
            // This list is further used by commit method
            USAAInf_LogHelper.lstLogWrapper.add(logInstance); 
        }
        //Return null on error or is DML was not executed due to parameters.
        return null;
    }  
    
   /* 
    * Method to commit log records stored in lstLogWrapper based on value in boolean variable executeNow.
    * @param     Boolean     Flag to control whether log needs to be commited     
    * @return    void        Method doesn't return any value.               
    */
    public static void commitLogs(Boolean executeNow) 
    {
        // Verifying whether log needs to inserted or not
        if (executeNow) 
        {
            USAAInf_LogHelper.commitLogsNow();
        } 
        else 
        {
            // Serialize lstLogWrapper to JSON and commit logs in future context
            String lstLogWrapperJSON = USAAInf_LogWrapper.encodeJSON(USAAInf_LogHelper.lstLogWrapper); 
            system.debug(lstLogWrapperJSON);
            USAAInf_LogHelper.commitLogsInFuture(lstLogWrapperJSON);
        }
    }  
    
    /* 
    * Method returns an instance of LogWrapper with values from parameters passed to the method.
    * @param     String                Contains error Type value
    * @param     String                Contains Severity value
    * @param     String                Contains Source Type value
    * @param     String                Contains LogSource value
    * @param     String                Contains LogDetails value
    * @param     String                Contains LogMessage value
    * @param     String                Contains MethodName value
    * @param     String                Contains Recordid value
    * @param     String                Contains Object Nam value
    * @param     String                Contains PayloadFile value
    * @return    USAAInf_LogWrapper    Method inserts a instance of wrapper USAAInf_LogWrapper               
    */
    public static USAAInf_LogWrapper returnLogs(String logType, String logSeverity, String logSourceType, 
                                             String logSource, String logDetails, String logMessage, 
                                             String logPayloadFile,String methodname, String recordId, 
                                             String Objectname) 
    {
        // Creating instace of log object and populating values
        USAAInf_LogWrapper logInstance= new USAAInf_LogWrapper();
        logInstance.logType = logType;
        logInstance.logSource = logSource;
        logInstance.logDetails = logDetails;
        logInstance.logMessage = logMessage;
        logInstance.logSeverity = logSeverity;
        logInstance.logTimeStamp = System.now();
        logInstance.logSourceType = logSourceType;
        logInstance.logPayloadFile = logPayloadFile;
        logInstance.Recordid=recordid;
        logInstance.Objectname=Objectname;
        logInstance.Methodname=methodname;
        
        // returning log instance 
        return logInstance;
    } 
    
    /*   
    * <p> This method commits log details to USAAInf_Log__c object
    *
    * @param     List<USAAInf_LogWrapper>               List of USAAInf_LogWrapper records which needs to be committed
    * @return    void                                   Method doesn't return any value.               
    */
    public static void commitLogs(List<USAAInf_LogWrapper> logWrapperList) 
    {
        // Instance of log wrapper 
        USAAInf_LogWrapper logwrapper;
        
        // Instance of attachment created via file
        Attachment attachmentFileInstance;
        
        // Instance of attachmetn created via Message 
        Attachment attachmentMessageInstance;
        
        // Boolean flag to identify whether all logs created or nto
        Boolean allLogsCreated;
        
        // Set to store Sucess and failure Ids
        Set<Id> successIDs = new Set<Id>();
        Set<Id> failureIDs = new Set<Id>();
        
        // List to store log records which needs to be inserted
        List<SObject> lstLogs = new List<SObject>();
        List<Attachment> lstAttachments = new List<Attachment>();
        //check type of log for each list
        if (!logWrapperList.isEmpty()) 
        {          
            // Creating list of logs from the wrapper     
            lstLogs = USAAInf_LogHelper.getObjectInstances(logWrapperList);

            if (!lstLogs.isEmpty()) 
            {
                if (!USAAInf_LogHelper.getIsLoggingEnabled() && !USAAInf_LogHelper.getIsIntegrationLoggingEnabled()) 
                {
                    for (Integer i = 0; i < lstLogs.size(); i++) 
                    {
                        //remove transaction logs to prevent from being saved to Log Object
                        USAAInf_Log__c log=(USAAInf_Log__c)lstLogs[i];
                        if (log.Type__c != UWC_GlobalConstants.INTEGRATION) 
                        {
                            lstLogs.remove(i);
                        }
                    }
                }
            } 
            // If there are logs to be inserted then insert to log object and capture the result set
            if (!lstLogs.isEmpty()) 
            {
                Database.SaveResult[] lstSaveResults = Database.insert(lstLogs, false);
                String strErrorDetails;
                for (Integer intSaveResultCount = 0; intSaveResultCount < lstSaveResults.size(); intSaveResultCount++) 
                {
                    Database.SaveResult objSaveResult = lstSaveResults[intSaveResultCount];
                    if (objSaveResult.isSuccess()) 
                    {
                        successIDs.add(objSaveResult.getId());
                    }
                    else 
                    {
                        for (Database.Error objInsertError : objSaveResult.getErrors())
                        {
                            strErrorDetails += objInsertError.getMessage() + UWC_GlobalConstants.NEWLINE;
                        }
                        //Add an enter at the end of each LOG record end.
                        strErrorDetails += UWC_GlobalConstants.ENDLINE;
                    }
                }
                if (String.isNotBlank(strErrorDetails)) 
                {                   
                    USAAInf_LogUtility.createLogs(UWC_GlobalConstants.EXCEPTION_TYPE,UWC_GlobalConstants.EXCEPTION_SEVERITY,UWC_GlobalConstants.EXCEPTION_SOURCETYPE,UWC_GlobalConstants.LOGUTILITY,strErrorDetails,'',UWC_GlobalConstants.EXCEPTION_DLOGMETHODCOMMIT,'','','',true); 
                }
                // If successIds is not empty then an create a list of attachments 
                if (!successIDs.isEmpty()) 
                {
                    for (Integer i = 0; i < logWrapperList.size(); i++) 
                    {
                        USAAInf_Log__c log = (USAAInf_Log__c)lstLogs[i];                           
                        logwrapper = logWrapperList[i];
                        attachmentFileInstance = new Attachment();
                        attachmentMessageInstance = new Attachment();
                        if (successIDs.contains(log.Id)) {
                            if ((log.Id != null) && (log.Type__c == UWC_GlobalConstants.INTEGRATION)
                                    && String.isNotBlank(logwrapper.logPayloadFile)) 
                            {
                                attachmentFileInstance = USAAInf_LogHelper.returnAttachmentFromPayload(log, logwrapper);
                                lstAttachments.add(attachmentFileInstance);
                            }
                            
                        }
                    }
                }
                // insert attachments if list is not empty
                if (!lstAttachments.isEmpty()) 
                {
                    Database.SaveResult[] srAttList = Database.insert(lstAttachments, false);
                }
            }            
        } 
    } 
    
    /*  
    * <p> 
    * Method to process Database.SaveResult[] from partial DML inserts or updates. 
    * All Database.SaveResult errors are committed in a single transaction to prevent
    * the DML 150 statement governor limit
    * 
    * @param    Database.SaveResult[]    List of Database save results
    * @param    String                   Source Type
    * @param    String                   Class Name
    * @param    String                   Function name
    * @param    String                   Object Name
    * @return   void                     No value is returned
    */
    public static void insertLogTransaction(Database.SaveResult[] resultList,String sourceType,String className,String funcName,String objectName)
    {        
        //Iterate through each Database.SaveResult
        for(Database.SaveResult result : resultList)
        {
            if(!result.isSuccess())
            {
                //Create log for each error
                    processDatabaseErrors(result.getErrors(),sourceType, className, funcName, objectName, result.getId());
            }
        }
        
        //Commit all error logs 
        USAAInf_LogUtility.commitLogs(true);
    }
    
    
   /*  
    * <p> 
    * Method to process Database.DeleteResult[] from partial DML deletes. 
    * All Database.DeleteResult errors are committed in a single transaction to prevent
    * the DML 150 statement governor limit
    * 
    * @param    Database.DeleteResult[]  List of Database delete results
    * @param    String                   Source Type
    * @param    String                   Class Name
    * @param    String                   Function name
    * @param    String                   Object Name
    * @return   void                     No value is returned
    */
    public static void deleteLogTransaction(Database.DeleteResult[] resultList,String sourceType,String className,String funcName,String objectName)
    {
        //Iterate through each Database.DeleteResult
        for(Database.DeleteResult result : resultList)
        {
            if(!result.isSuccess())
            {
                //Create log for each error
                processDatabaseErrors(result.getErrors(),sourceType, className, funcName, objectName, result.getId());
            }
        }
        
        //Commit all error logs 
        USAAInf_LogUtility.commitLogs(true);
    }
    
   /*  
    * <p> 
    * Helper Method to process Database.Error entries and create logs without a commit
    * 
    * @param    Database.Error[]         List of Database errors
    * @param    String                   Source Type
    * @param    String                   Class Name
    * @param    String                   Function name
    * @param    String                   Object Name
    * @return   void                     No value is returned
    */
    private static void processDatabaseErrors(List<Database.Error> errorList,String sourceType,String className,String funcName,String objectName,String recordId)
    {
        //Process Database Errors
        for(Database.Error error : errorList)
        {
            String errorFields = '';
            for(String fieldName : error.getFields())
            {
                errorFields += fieldName + ',';
            }
            
            //Do not immediately commit logs to avoid governor limit
            USAAInf_LogUtility.createLogs(
                UWC_GlobalConstants.EXCEPTION_TYPE,
                UWC_GlobalConstants.EXCEPTION_SEVERITY,
                sourceType,
                className,
                errorFields,
                error.getMessage(),
                funcName,
                recordId,
                objectName,
                null,
                false
            );
        }
    }
    
}