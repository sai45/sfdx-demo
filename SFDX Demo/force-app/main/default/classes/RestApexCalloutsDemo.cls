public class RestApexCalloutsDemo {

    public static String Base_URL = 'https://demo6512258.mockable.io/getData';
    public static void getDemoApiData(){
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint(Base_URL);
        req.setHeader('Content-Type', 'application/json');
        
        HttpResponse resp = new Http().send(req);
        System.debug(resp.getBody());
    }
}