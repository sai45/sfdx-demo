public with sharing class OpportunitySplit {

    @AuraEnabled
    public static List<User> getSalesRepList(){
        List<User> lstUsers = [Select Id,Name from User];
        return lstUsers;
    }
    
    @AuraEnabled
    public static List<Account> getBrokerAgencyList(){
        List<Account> lstAccounts = [Select Id,Name from Account];
        return lstAccounts;
    }
    
    @AuraEnabled
    public static List<Contact> getBrokerIndividualList(){
        List<Contact> lstContacts = [Select Id,Name from Contact];
        return lstContacts;
    }
    
    @AuraEnabled
    public static List<sObject> getObjectDetails(String searchKeyWord, String ObjectName){
        System.debug(searchKeyWord+'-->'+ObjectName);
        String searchKey = searchKeyWord + '%';
      
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        String sQuery =  'select Id, Name from ' +ObjectName + ' where Name LIKE: searchKey order by createdDate DESC limit 5';
        List <sObject> lstOfRecords = Database.query(sQuery);
        System.debug('lstOfRecords ->'+lstOfRecords);
        return lstOfRecords;
    }
    
    @AuraEnabled
    public static void UpsertCommissions(String commissions){
        
    }
    
    public class Commissions{
        
    }
    
    public class SalesRep{
        public String Id;
        public String Sales_Rep;
        public String Split_Percentage;
        
        public SalesRep(String Id,String Sales_Rep,String Split_Percentage){
            this.Id = Id;
            this.Sales_Rep = Sales_Rep;
            this.Split_Percentage = Split_Percentage;
        }
    }
    
}