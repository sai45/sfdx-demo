/*******************************************************************************************************************************

* Copyright(C)2017
* USAA
* All Rights Reserved
* Name    :USAAInf_LogUtilityTest
* Purpose  :This class is used to test the Log Utility class functionality
* History

**********************************************************************************************************************************
*   Version No                  Name                              Date                                Description

* ----------------------------------------------------------------------------------------------------------------------------
1.0                   Amanjot Kaur                     05/26/2017                          Initial Version
*                              
2.0                  Harshith M L/                     08/30/2017                         Bringing DLOG framework to USAA Standards 
                     Amanjot Kaur     
3.0                  Zach McKie                        09/18/2017                         Added test cases for bulk logging of transactions
***********************************************************************************************************************************/


@isTest
public class USAAInf_LogUtilityTest
{   
    /*
    * <p>    
    * This method is responsible to load test data
    * @return   void    Doesn't return any value
    */    
    @testSetup
    private static void loadData()
    {
        //Controller Setting to store Transaction Logs if exception occurs
        USAAInf_ControllerSettings__c switchSetting = USAAInf_ControllerSettings__c.getInstance();
        switchSetting.LogIntegrationErrors__c=false; 
        switchSetting.LogsEnabled__c=true;
        insert switchSetting;
        
        //Controller Setting to store Integration Logs if exception occurs
        USAAInf_ControllerSettings__c switchSetting2 = new USAAInf_ControllerSettings__c();
        switchSetting2.LogIntegrationErrors__c=true;
        switchSetting2.LogsEnabled__c=false;
        insert switchSetting2;
        
    }
    
    /*
    * <p>    
    * This method is responsible to test logutility transaction exception handling logic
    * @return   void    Doesn't return any value
    */  
    static testMethod void testLogTransaction()
    {
        //Instance of Utility class
        USAAInf_LogUtility logutilityinstance=new USAAInf_LogUtility();
        
        USAAInf_ControllerSettings__c switchSetting =[Select LogIntegrationErrors__c,LogsEnabled__c from USAAInf_ControllerSettings__c 
                                                     where LogIntegrationErrors__c=false LIMIT 1];
        
        //running test as logged in user
        User currentuser=[Select id from user where id=:UserInfo.getUserId()];
        
        System.runAs(currentuser) 
        {
            USAAInf_LogUtility.createLogs('TestMethod','ALERT','TestClass','LogUtility_Test','test','Test','testmetjod01',' ','Account','',True);
            USAAInf_LogUtility.createLogs('Integration','ALERT','TestClass','LogUtility_Test','test','Test','testmetjod01',' ','Account','',false);
            USAAInf_LogUtility.commitLogs(false);
            USAAInf_Log__c testlog=[select id,Source__c,Type__c from USAAInf_Log__c lIMIT 1];
            //create a errormessage with more than 2000 characters
            String errorMessage = '';
            for(Integer i = 0; i < 1000; i++)
            {
                errorMessage = errorMessage + i;
            }         
            List<USAAInf_LogWrapper> wrapperlist=new List<USAAInf_LogWrapper>();
            
            USAAInf_LogWrapper logObj = new USAAInf_LogWrapper();
            logObj.logType = 'Integration';
            logObj.logSource = 'Apex';
            logObj.logMessage = errorMessage;
            logObj.logTimeStamp = System.now();
            logObj.logPayloadFile = errorMessage;
            wrapperlist.add(logObj);
            
            USAAInf_LogWrapper logObj1 = new USAAInf_LogWrapper();
            logObj1.logType = 'Integration';
            logObj1.logSource = 'Apex';
            logObj1.logMessage = errorMessage;
            logObj1.logTimeStamp = System.now();
            logObj1.logPayloadFile = 'test';
            
            USAAInf_LogHelper.createAttachmentFromPayload(testlog,logObj);
            USAAInf_LogHelper.returnAttachmentFromPayload(testlog,logObj);
            USAAInf_LogHelper.createAttachmentFromMessage(testlog,errorMessage);
            USAAInf_LogHelper.getIsIntegrationLoggingEnabled();
            USAAInf_LogUtility.returnLogs('Error', 'ALERT', 'Apex Class', 'Test Class', 'Test Error', errorMessage, errorMessage, 'Test Method', NULL, 'Account');
        }
        //Test of Dlog records have been inserted or not
        system.assertEquals(1, [SELECT Id FROM USAAInf_Log__c].size());
    }
    
    /*
    * <p>    
    * This method is responsible to test logutility integration exception handling logic
    * @return   void
    */  
    static testMethod void testLogIntegration()
    {
        // Variable to store list of contacts
        List<Contact> testcon=new List<Contact>();
        for(Integer i=0;i<5;i++)
        {
            Contact c=new Contact();
            testcon.add(c);
        }
        
        // Inserting contact which will hit exception
        Database.SaveResult[] testconinsert=Database.insert(testcon,false);
        
        // Fetching and updating custom setting
        USAAInf_ControllerSettings__c switchSetting =[Select LogIntegrationErrors__c,LogsEnabled__c from 
                                                    USAAInf_ControllerSettings__c where LogIntegrationErrors__c=true 
                                                    LIMIT 1];
        switchSetting.LogIntegrationErrors__c=false;
        switchSetting.LogsEnabled__c=false;
        update switchSetting;
        
        //create a errormessage with more than 2000 characters
        String errorMessage = '';
        for(Integer i = 0; i < 1000; i++)
        {
            errorMessage = errorMessage + i;
        }
        // Creating logs and asserting for log records
        USAAInf_LogUtility.createLogs('TestMethod','ALERT','TestClass','LogUtility_Test','test','TEST','testmethod01',' ','Account','',false);
        USAAInf_LogUtility.commitLogs(true);
        USAAInf_LogUtility.insertLogTransaction(testconinsert,'TestClass','LogUtility_Test','test','Test');
        USAAInf_LogUtility.createLogs('TestMethod','ALERT','TestClass','LogUtility_Test', 'test','TEST','testmethod01',' ','Account', errorMessage,True);
        USAAInf_LogUtility.createLogs('TestMethod','TEST_SEVERITY','TestClass','LogUtility_Test', 'test','TEST','testmethod01',' ','Account', errorMessage,false); 
        USAAInf_LogUtility.commitLogs(true);
        //Test of Dlog records have been inserted or not
        system.assertEquals(8, [SELECT Id FROM USAAInf_Log__c].size());
    }
    
    /*
     * <p> 
     * Method to test insertLogTransaction with all records failing during 
     * a bulk partial insert
     * 
     * @return void
     */
    static testMethod void testInsertLogTransactionForFailedInserts()
    {
        //Mock 200 Contact records missing required fields
        List<Contact> contactList = new List<Contact>();
        for(Integer i = 0; i < 200 ; i++)
        {
            Contact testContact = new Contact();
            contactList.add(testContact);
        }
        
        //Bulk partial insert Mock records
        Database.SaveResult[] resultList = Database.insert(contactList, false);
        
        //Execute Test
        Test.startTest();
        USAAInf_LogUtility.insertLogTransaction(resultList,UWC_GlobalConstants.EXCEPTION_SOURCETYPE,'USAAInf_LogUtilityTest', 'testBulkInsertLogTransactionForFailedInserts','Contact');
        Test.stopTest();
        
        //Assert all 200 records failed to insert
        System.assertEquals(200, [SELECT Id FROM USAAInf_Log__c 
                                  WHERE Source__c = 'USAAInf_LogUtilityTest' AND Method__c = 'testBulkInsertLogTransactionForFailedInserts'].size());
    }
    
    /*
     * <p> 
     * Method to test bulkDeleteLogTransaction with all records failing during 
     * a bulk partial delete
     * 
     * @return void
     */
    static testMethod void testDeleteLogTransactionForFailedDeletes()
    {
        //Mock 200 Contact records missing required fields
        List<Contact> contactList = new List<Contact>();
        for(Integer i = 0; i < 200 ; i++)
        {
            Contact testContact = new Contact(LastName = 'McKie');
            contactList.add(testContact);
        }
        insert contactList;
        
        //Delete the Mock records to force errors during the second delete
        delete contactList;
        
        //Bulk partial delete Mock records
        Database.DeleteResult[] resultList = Database.delete(contactList, false);
        
        //Execute Test
        Test.startTest();
        USAAInf_LogUtility.deleteLogTransaction(
            resultList, 
            UWC_GlobalConstants.EXCEPTION_SOURCETYPE, 
            'USAAInf_LogUtilityTest', 
            'testBulkDeleteLogTransactionForFailedDeletes', 
            'Contact'
        );
        Test.stopTest();
        
        //Assert all 200 records failed to delete the second time
        System.assertEquals(200, [SELECT Id FROM USAAInf_Log__c 
                                  WHERE Source__c = 'USAAInf_LogUtilityTest' AND Method__c = 'testBulkDeleteLogTransactionForFailedDeletes'].size());
    }
}