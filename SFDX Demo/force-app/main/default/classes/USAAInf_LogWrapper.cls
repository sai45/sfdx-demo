/********************************************************************************
 * Copyright (C) 2017
 * USAA
 * All Rights Reserved
Apex Class Name : USAAInf_LogWrapper
Version         : 1.0
Created On      : August 18 2017
Function        : This class is used for inserting exceptions in DLOG
TestClass       : USAAInf_LogutilityTest


Modification Log :

*     Version No                    Developer                         Date                                Comments
*
* ----------------------------------------------------------------------------------------------------------------------------
*          1.0                  Amanjot Kaur                        08/18/2017                          Initial Version
*                               
           2.0                  Amanjot Kaur                        08/28/2017                         Implemented Code Review Comments #120899
           3.0                  Harshith M L/                       08/30/2017                         Bringing DLOG framework to USAA Standards 
                                Amanjot Kaur  
*************************************************************************************************************/
public class USAAInf_LogWrapper
{  
    //variable to hold Exception Type
    public String logType {get; set;}
    // Variable to hold object name for which exception will occur
    public String logObject {get; set;}
    //Variable to hold Source of Exception
    public String logSource {get; set;}
    //Variable to hold Exception details
    public String logDetails {get; set;}
    // Variable to hold Error Message thrown during Exception
    public String logMessage {get; set;}
    // Variable to hold Severity Of Exception
    public String logSeverity {get; set;}
    //Variable to hold component Type that Throw Exception
    public String logSourceType {get; set;}
    // Variable to store Exception details during Integration
    public String logPayloadFile {get;set;}
    //Variable to store log time 
    public Datetime logTimeStamp {get; set;}
    //Variable to Store Recordid for which transaction exception occured
    public String Recordid{get;set;}
    // Variable to hold object name for which exception will occur
    public String Objectname{get;set;}
    // Variable to hold Method name for which exception will occur
    public String Methodname{get;set;}
    
    /*
    * <p>    
    * This method is going to be called from LogHelper to serialize and returns JSON
    * @param    List<USAAInf_LogWrapper>  Wrapper List instances    
    * @return   String              This method returns wrapper as JSON.
    */
    public static String encodeJSON(List<USAAInf_LogWrapper> wrappedLogs)
    {
        String wrappedLogsJSON = '';
        
        // Serializing list of USAAInf_LogWrapper to String
        if(!wrappedLogs.isEmpty())
        {
            wrappedLogsJSON = JSON.serialize(wrappedLogs);
        }

        return wrappedLogsJSON;
    }
    
    /*
    * <p>    
    * This method is going to be called from LogHelper which deserialized and send list of USAAInf_LogWrapper
    * @param    String                JSON String of wrapper     
    * @return   List<USAAInf_LogWrapper> This method returns List of wrapper.
    */

    public static List<USAAInf_LogWrapper> decodeJSON(String wrappedLogsJSON)
    {
        List<USAAInf_LogWrapper> wrappedLogs = new List<USAAInf_LogWrapper>();
        
        // Deserializing String to list of USAAInf_LogWrapper
        if(!String.isBlank(wrappedLogsJSON))
        {
            wrappedLogs = (List<USAAInf_LogWrapper>) JSON.deserialize(wrappedLogsJSON, List<USAAInf_LogWrapper>.class);
        }
        
        return wrappedLogs;
    }
}