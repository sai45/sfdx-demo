/*******************************************************************************************************************************

* Copyright(C)2017
* USAA
* All Rights Reserved
* Name      :UWC_GlobalConstants
* Purpose   :To Hold all the Constants used across the Apex Code
* History
******************************************************************************************************************************************************************
*   Version No                  Name                                Date                                Description
* ----------------------------------------------------------------------------------------------------------------------------------------------------------------
         1.0                    Samridh Manucha                     09/17/2017                          Initial Version 
*        2.0                    Ravi Pandey                         12/01/2017                          Added Variable for queue availability and Constructor                                
*        3.0                    Kumar Amit                          02/09/2018                          Added variable for rest api  
*        4.0                    Gaurav Yadav                        02/16/2018                          Removed some constants moved to UWC_GlobalMessage metaData
*        5.0                    Gaurav Yadav                        03/07/2018                          Added in-progress status
*******************************************************************************************************************************************************************/
public class UWC_GlobalConstants 
{
      
     public UWC_GlobalConstants(){
      
     }
    //Dlog framework constants
    public static final String INTEGRATION='Integration';
    public static final String LOGUTILITY='USAAInf_Logutility'; 
    public static final String EXCEPTION_DLOGMETHODCOMMIT='commitLogs';
    public static final String ENDLINE='\n----------------------------------\n';
    public static final String NEWLINE='\n >';
    public static final String ERRORMSG='The list to commit is empty. Please add logs prior to commiting.';
    public static final String LOGHELPER='USAAInf_LOGHelper Class';
    public static final String EXCEPTION_DLOGMETHODATTACHMENT='createAttachmentFromMessage';
    public static final String OBJECTLOG='USAAInf_Log';
    public static final String CONTENTTYPE='application/octet-stream';
    public static final String SOURCE='Source__c';
    public static final String DETAILEDMSG=' - detailed log message.txt';
    public static final String DETAILEdLOGMESSAGE=' with detailed log message.';
    public static final String ATTACHMENTMESSAGE='Attachment created for ';
    public static final String LOGMETHODATTACHPAYLOAD='createAttachmentFromPayload';
    public static final String INTEGRATIONREQUESTDETAILS='- integration request details.txt';
    public static final String DESCRIPTIONMESSAGE=' during execution of ';
    public static final String SEVERITYERRORMSG='The mapping is incorrect in LoggingFrameWork_LogTypes_Severity Custom Label';    
    //
    public static final String EXCEPTION_TYPE='ERROR';
    public static final String EXCEPTION_SEVERITY='ALERT';
    public static final String EXCEPTION_SOURCETYPE='APEX CLASS';
    
    
    //Variable for queue availablity

    public static final String STR_WAITING='waiting';
    public static final String STR_PRIMARY='primaryButton';
    public static final String STR_SECONDARY='secondaryButton';
    public static final String STR_LIVEAGENTAPIVERSION='X-LIVEAGENT-API-VERSION';
    public static final String STR_SETAPIVERSION='41';
    public static final String STR_GETMETHOD='GET';
    
    //Variable for UWC_HackerService
    public static final String UWC_HackerService = 'UWC_HackerService';
    
    //variable for rest api
    public static final String STR_POSTMETHOD='POST';
    public static final String STR_CONTENTTYPE='Content-Type';
    public static final String STR_JSONAPPLICATION='application/json';
    
    
    //Variable for UWC_LiveChatTranscriptTriggerHandler
    public static final String STR_COMPLETED='Completed';
    public static final String STR_MISSED='Missed';
    public static final String STR_BLOCKED='Blocked';
    public static final String STR_DROPPED='Dropped';
    public static final String STR_INPROGRESS='InProgress';
    
    
    //Variable for UWC_LiveChatTranscriptTriggerservice
    public static final String STR_AUTODECLINESECURITY='Auto Decline - Security';
    
    
    //Variable for UWC_QueueAvailability
    public static final String STR_AVAILABLE='Available';
    
    
     //Variable for UWC_QueueCapacity
    public static final String STR_QUEUE='queue';
     public static final String STR_REGULAR='Regular';
     public static final String STR_ESCALATION='Escalation';
     public static final String STR_OPENED='opened';
     
     
     //Variable for UWC_Utility
    public static final String STR_TRANSBODY='TransBody';
    public static final String STR_MEMBERNAME='MemberName';
    public static final String STR_MEMBERNUMBER='MemberNumber';
    public static final String STR_TEST='Test';
    
    
    //Variable for UWC_pdfContent
    public static final String STR_HTMLBODY='<HTML><BODY>';
    public static final String STR_BODYHTML='</BODY></HTML>';
    public static final String STR_BODY='Body';
    public static final String STR_H2='<H2>Transcript Information in PDF</H2>';
    public static final String STR_LIVECHATTRANSCRIPT='LiveChatTranscript';
    public static final String STR_PARAOPENING='<P>';
    public static final String STR_PARACLOSING='</P>';
    public static final String PARAMESSAGE='THERE WAS AN ERROR GENERATING PDF: ' ;
    
    
    
    //Messages for CEO relation
    //public static final String MAIL_SENT_SUCCESS = 'Mail Sent Successfully along with Live Chat Transcipt Name :';
    public static final String STR_PDFAPPLICATION='application/pdf';
    public static final String STR_PDF='.pdf';
    public static final String STR_FILTER='FILTER';
}