@RestResource(urlMapping = '/AccountData/*')
global class AccountRestApiData {

    @HttpGet
    global static List<Account> getAccountDetails(){
        List<Account> accountDetails = [Select Id,Name from Account Limit 15];
        return accountDetails;
    }
    
    @HttpPost
    global static ResultClass createNewAccount(String AccountName){
        Account acct = new Account(Name = AccountName);
        Database.SaveResult result = Database.insert(acct);
        ResultClass res = new ResultClass(result.getId(), result.isSuccess(), result.getErrors());
        return res;
    }
    
    global class ResultClass{
        public String Id;
        public boolean IsSuccess;
        public List<String> errors;
        
        public ResultClass(String Id,boolean IsSuccess,List<Database.Error> errorList){
            this.Id = Id;
            this.IsSuccess = IsSuccess;
            errors = new List<String>();
            for(Database.error err :errorList){
                errors.add(err.getMessage());
            }
        }
    }
}