({
	 init: function (cmp, event, helper) {
        cmp.set('v.mapMarkers', [
            {
                location: {
                    Latitude: '37.790197',
                    Longitude: '-122.396879'
                },

                title: 'The White House',
                description: 'Landmark, historic home & office of the United States president, with tours for visitors.'
            }
        ]);
        cmp.set('v.zoomLevel', 16);
    },
})