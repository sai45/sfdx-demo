({
    getParameterByName: function(component, event, name) {
        name = name.replace(/[\[\]]/g, "\\$&");
        var url = window.location.href;
        var regex = new RegExp("[?&]" + name + "(=1\.([^&#]*)|&|#|$)");
        var results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },
	getSalesRepList : function(component, helper) {
		var action = component.get('c.getSalesRepList');
        action.setCallback(this, function(response){
            var responseState = response.getState();
            if(responseState === 'SUCCESS' && response.getReturnValue() != undefined && response.getReturnValue() != null && response.getReturnValue() != '') {
                component.set('v.SalesRep',response.getReturnValue());
            }else {
                console.log('error ', response.getError());
            }
        });
        $A.enqueueAction(action);
	},
    getBrokerAgencyList : function(component, helper) {
		var action = component.get('c.getBrokerAgencyList');
        action.setCallback(this, function(response){
            var responseState = response.getState();
            if(responseState === 'SUCCESS' && response.getReturnValue() != undefined && response.getReturnValue() != null && response.getReturnValue() != '') {
                component.set('v.BrokerAgencies',response.getReturnValue());
            }else {
                console.log('error ', response.getError());
            }
        });
        $A.enqueueAction(action);
	},
    getBrokerIndividualList : function(component, helper) {
		var action = component.get('c.getBrokerIndividualList');
        action.setCallback(this, function(response){
            var responseState = response.getState();
            if(responseState === 'SUCCESS' && response.getReturnValue() != undefined && response.getReturnValue() != null && response.getReturnValue() != '') {
                component.set('v.BrokerIndividual',response.getReturnValue());
            }else {
                console.log('error ', response.getError());
            }
        });
        $A.enqueueAction(action);
	}
})