({
    doInit : function(component, event, helper) {
        var value = helper.getParameterByName(component , event, 'inContextOfRef');
        var context = JSON.parse(window.atob(value));
        component.set("v.parentRecordId", context.attributes.recordId);
    },
    selectTab: function(component, event, helper){
        var selected = component.get("v.key");
        console.log(component.get('v.OpportunitySplits'));
    },
    closeModel: function(component, event, helper){
        var homeEvt = $A.get("e.force:navigateToSObject");
        homeEvt.setParams({
            "recordId": component.get('v.recordId'),
            "slideDevName": "Detail",
        });
        homeEvt.fire();
    },
    validateData: function(component, event, helper){
    	var splitData = component.get('v.OpportunitySplits');
    }
})