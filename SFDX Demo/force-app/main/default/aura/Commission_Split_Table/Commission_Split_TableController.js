({
	doInit : function(component, event, helper) {
        var opportunitySplit = component.get('v.OpportunitySplits');
        opportunitySplit = {
            "SalesRepresentative": [{
                "SalesRep":"",
                "SplitPercentage": ''
            }],
            "BrokerageIndividual_Agency":[{
                "BrokerageAgency": "",
                "BrokerageIndividual": "",
                "SplitPercentage": ''
            }],
            "BCBS":[{
                "Blue_Cross_B_S":"",
                "SplitPercentage": ''
            }]
        };
        component.set('v.OpportunitySplits', opportunitySplit);
		helper.getSalesRepList(component,helper);
        helper.getBrokerAgencyList(component,helper);
        helper.getBrokerIndividualList(component,helper);
	},
    addNewSplit : function(component, event, helper){
        var opportunitySplit = component.get('v.OpportunitySplits');
        var key = component.get('v.key');
        if(key == 'SR'){
            console.log(opportunitySplit.SalesRepresentative[0]);
            opportunitySplit.SalesRepresentative.push({
                "SalesRep":"",
                "SplitPercentage": ''
            });
            console.log(opportunitySplit.SalesRepresentative);
        }else if(key == 'BA_BI'){
            opportunitySplit.BrokerageIndividual_Agency.push({
                "BrokerageAgency": "",
                "BrokerageIndividual": "",
                "SplitPercentage": ''
            });
        }else if(key == 'BSBC'){
            opportunitySplit.BCBS.push({
                "Blue_Cross_B_S":"",
                "SplitPercentage": ''
            });
        }
        component.set('v.OpportunitySplits', opportunitySplit);
    },
    deleteSplit : function(component, event, helper){
        var opportunitySplit = component.get('v.OpportunitySplits');
        var key = component.get('v.key');
        if(key == 'SR'){
            opportunitySplit.SalesRepresentative.splice(event.currentTarget.getAttribute("data-pos")-1, 1);
        }else if(key == 'BA_BI'){
            opportunitySplit.BrokerageIndividual_Agency.splice(event.currentTarget.getAttribute("data-pos")-1, 1);
        }else if(key == 'BSBC'){
            opportunitySplit.BCBS.splice(event.currentTarget.getAttribute("data-pos")-1, 1);
        }
        component.set('v.OpportunitySplits', opportunitySplit);
    }
})