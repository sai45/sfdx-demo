({
    doInit : function(component, event, helper){
        component.set('v.columns', [
            {label: 'Document ID', fieldName: 'documentId', type: 'text'},
            {label: 'Document Type', fieldName: 'documentType', type: 'text'},
            {label: 'Document Name', fieldName: 'documentName', type: 'text'},
            {label: 'Document Link', fieldName: 'documentlink', type: 'url'}
        ]);
    },
	getCaseDataInfo : function(component, event, helper) {
        console.log(component.get('v.pageSize'));
		helper.getCaseDetails(component, helper);
	},
    onNext : function(component, event, helper) {        
        var pageNumber = component.get("v.currentPageNumber");
        component.set("v.currentPageNumber", pageNumber+1);
        $A.enqueueAction(component.get('c.getSearchList'));
        //helper.buildData(component, helper, null);
    },
    
    onPrev : function(component, event, helper) {        
        var pageNumber = component.get("v.currentPageNumber");
        component.set("v.currentPageNumber", pageNumber-1);
        $A.enqueueAction(component.get('c.getSearchList'));
        //helper.buildData(component, helper, null);
    },
    
    processMe : function(component, event, helper) {
        component.set("v.currentPageNumber", parseInt(event.target.name));
        $A.enqueueAction(component.get('c.getSearchList'));
        //helper.buildData(component, helper, null);
    },
    
    onFirst : function(component, event, helper) {        
        component.set("v.currentPageNumber", 1);
        $A.enqueueAction(component.get('c.getSearchList'));
        //helper.buildData(component, helper, null);
    },
    
    onLast : function(component, event, helper) {        
        component.set("v.currentPageNumber", component.get("v.totalPages"));
        $A.enqueueAction(component.get('c.getSearchList'));
        //helper.buildData(component, helper, null);
    },
    getSearchList : function(component, event, helper){
        console.log(component.get("v.pageSize"));
        var searchValue = component.get("v.searchKey");
        if(searchValue != null && searchValue.length > 1){
         	helper.buildData(component, helper, searchValue);  
        }else{
            helper.buildData(component, helper, null); 
        }
    }
})