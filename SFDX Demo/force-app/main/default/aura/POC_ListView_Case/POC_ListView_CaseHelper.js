({
	getCaseDetails : function(component,helper) {
        var objectJSON = [{ "documentId":"12awse6tggy","documentType":"Text","documentName":"Test1","documentlink":"https://login.salesforce.com"},
                        {"documentId":"24awse6tggy","documentType":"Text","documentName":"Test2","documentlink":"https://login.salesforce.com"},
                        {"documentId":"35awse6tggy","documentType":"Text","documentName":"Test3","documentlink":"https://login.salesforce.com"},
            			{ "documentId":"42awse6tggy","documentType":"Text","documentName":"Test1","documentlink":"https://login.salesforce.com"},
                        {"documentId":"54awse6tggy","documentType":"Text","documentName":"Test2","documentlink":"https://login.salesforce.com"},
                        { "documentId":"62awse6tggy","documentType":"Text","documentName":"Test1","documentlink":"https://login.salesforce.com"},
                        {"documentId":"74awse6tggy","documentType":"Text","documentName":"Test2","documentlink":"https://login.salesforce.com"},
                        {"documentId":"85awse6tggy","documentType":"Text","documentName":"Test3","documentlink":"https://login.salesforce.com"},
            			{ "documentId":"92awse6tggy","documentType":"Text","documentName":"Test1","documentlink":"https://login.salesforce.com"},
                        {"documentId":"04awse6tggy","documentType":"Text","documentName":"Test2","documentlink":"https://login.salesforce.com"},
            			{ "documentId":"12awse6tggy","documentType":"Text","documentName":"Test1","documentlink":"https://login.salesforce.com"},
                        {"documentId":"94awse6tggy","documentType":"Text","documentName":"Test2","documentlink":"https://login.salesforce.com"},
                        {"documentId":"85awse6tggy","documentType":"Text","documentName":"Test3","documentlink":"https://login.salesforce.com"},
            			{ "documentId":"72awse6tggy","documentType":"Text","documentName":"Test1","documentlink":"https://login.salesforce.com"},
                        {"documentId":"64awse6tggy","documentType":"Text","documentName":"Test2","documentlink":"https://login.salesforce.com"},
            			{ "documentId":"52awse6tggy","documentType":"Text","documentName":"Test1","documentlink":"https://login.salesforce.com"},
                        {"documentId":"44awse6tggy","documentType":"Text","documentName":"Test2","documentlink":"https://login.salesforce.com"},
                        {"documentId":"35awse6tggy","documentType":"Text","documentName":"Test3","documentlink":"https://login.salesforce.com"},
            			{ "documentId":"22awse6tggy","documentType":"Text","documentName":"Test1","documentlink":"https://login.salesforce.com"},
                        {"documentId":"14awse6tggy","documentType":"Text","documentName":"Test2","documentlink":"https://login.salesforce.com"}];

        component.set("v.totalPages", Math.ceil(objectJSON.length/parseInt(component.get("v.pageSize"))));
        component.set("v.isDataAvailable", objectJSON.length > 0 ? true : false);
        component.set("v.allData", objectJSON);
        component.set("v.currentPageNumber",1);
        helper.buildData(component, helper, null);
		//component.set("v.data", objectJSON);
	},
    buildData : function(component, helper, searchKey) {
        var data = [];
        var pageNumber = component.get("v.currentPageNumber");
        var pageSize = parseInt(component.get("v.pageSize"));
        var allData = component.get("v.allData");
        console.log(allData);
        var x = (pageNumber-1)*pageSize;
        var searchKeyResult = [];
        //loop for search key
        if(searchKey != null){
            console.log('Inside If ->'+searchKey);
            for(var i = 0; i < allData.length; i++){
                if(allData[i].documentId.toUpperCase().indexOf(searchKey.toUpperCase()) > -1 ||
                   allData[i].documentType.toUpperCase().indexOf(searchKey.toUpperCase()) > -1 ||
                   allData[i].documentName.toUpperCase().indexOf(searchKey.toUpperCase()) > -1){
                    searchKeyResult.push(allData[i]);
                }
            }
            console.log(searchKeyResult);
            component.set("v.totalPages", Math.ceil(searchKeyResult.length/parseInt(component.get("v.pageSize"))));
            for(; x<(pageNumber)*pageSize; x++){
                if(searchKeyResult[x]){
                    data.push(searchKeyResult[x]);
                }
            } 
            
        }else{
            //creating data-table data
            component.set("v.totalPages", Math.ceil(allData.length/parseInt(component.get("v.pageSize"))));
            for(; x<=(pageNumber)*pageSize - 1; x++){
                if(allData[x]){
                    data.push(allData[x]);
                }
            } 
        }
        console.log(data);
        component.set("v.data", data);
        
        helper.generatePageList(component, pageNumber);
    },
    generatePageList : function(component, pageNumber){
        pageNumber = parseInt(pageNumber);
        var pageList = [];
        var totalPages = component.get("v.totalPages");
        if(totalPages > 1){
            if(totalPages <= 10){
                var counter = 2;
                for(; counter < (totalPages); counter++){
                    pageList.push(counter);
                } 
            } else{
                if(pageNumber < 5){
                    pageList.push(2, 3, 4, 5, 6);
                } else{
                    if(pageNumber>(totalPages-5)){
                        pageList.push(totalPages-5, totalPages-4, totalPages-3, totalPages-2, totalPages-1);
                    } else{
                        pageList.push(pageNumber-2, pageNumber-1, pageNumber, pageNumber+1, pageNumber+2);
                    }
                }
            }
        }
        component.set("v.pageList", pageList);
    }
})