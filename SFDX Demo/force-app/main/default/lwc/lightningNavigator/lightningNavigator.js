import { NavigationMixin } from 'lightning/navigation'
import { LightningElement } from 'lwc';

export default class LightningNavigator extends NavigationMixin(LightningElement) {
    navigationHome(){
        this[NavigationMixin.Navigate]({
            type: 'standard__namedPage',
            attributes:{
                pageName: 'home'
            }
        });
    }

    navigationCase(){
        this[NavigationMixin.Navigate]({
            type: 'standard__objectPage',
            attributes:{
                objectApiName: 'Case',
                actionName: 'list'
            },
            state:{
                filterName: 'All Open Cases'
            }
        })
    }
}