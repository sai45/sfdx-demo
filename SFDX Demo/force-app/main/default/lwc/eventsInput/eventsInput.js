import { LightningElement, track } from 'lwc';

export default class EventsInput extends LightningElement {
    @track value;

    handleInput(event){
        const myValue = event.target.value;
        this.dispatchEvent(new CustomEvent('value',{
            detail: myValue
            //bubbles: true
        }));
    }
}