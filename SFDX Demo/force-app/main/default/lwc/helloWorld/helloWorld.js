import { LightningElement, track } from 'lwc';

export default class HelloWorld extends LightningElement {
    @track greeting = 'World';

    handleChange(event){
        const keyval = event.keyCode === 13;
        if(keyval){
            this.greeting = event.target.value;
        }
    }
}