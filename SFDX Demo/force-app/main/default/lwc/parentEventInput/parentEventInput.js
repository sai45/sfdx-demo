import { LightningElement, track } from 'lwc';

export default class ParentEventInput extends LightningElement {
    @track inputvalue;

    valueChange(event){
        this.inputvalue = event.detail;
    }

    /*constructor(){
        super();
        this.template.addEventListener('value', this.valueChange.bind(this));
    }*/
}