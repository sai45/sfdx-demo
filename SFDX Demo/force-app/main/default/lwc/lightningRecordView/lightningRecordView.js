import { LightningElement, api } from 'lwc';

export default class LightningRecordView extends LightningElement {
    @api recordId;

    handleReset() {
        const inputFields = this.template.querySelectorAll(
            'lightning-input-field'
        );
        if (inputFields) {
            inputFields.forEach(field => {
                field.reset();
            });
        }
     }
}